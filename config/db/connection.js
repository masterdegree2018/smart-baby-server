import Sequelize from 'sequelize';

const sequelize = new Sequelize('sb_development', 'dev', 'dev', {
    host: 'localhost',
    dialect: 'postgres'
});

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection with DB success!');
        sequelize.sync({
            force: false
        });
    })
    .catch((e) => {
        console.log('Cat not connect with DB!' + e);
    });

export default sequelize;
