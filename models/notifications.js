import Sequelize from 'sequelize';
import sequelize from '../config/db/connection';

const Notification = sequelize.define('notification', {
    message: Sequelize.TEXT,
    sender: Sequelize.STRING
});

export default Notification;
