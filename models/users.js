import Sequelize from 'sequelize';
import bcrypt from 'bcrypt-nodejs';
import sequelize from '../config/db/connection';
import Children from './Children';
import Notification from './notifications';

const Users = sequelize.define('users', {
    uniqueId: {
        type: Sequelize.STRING,
        allowNull: false
    },
    login: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    phone: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    name: {
        type: Sequelize.STRING,
        allowNull: true
    },
    role: {
        type: Sequelize.ENUM('ADMIN', 'USER'),
        allowNull: false
    },
    activity: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: new Date()
    },
    numberOfChildren: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    token: {
        type: Sequelize.JSON,
        defaultValue: {}
    }
});

Users.hasMany(Children);
Users.belongsTo(Children, { as: 'ChildrenConnection', constraints: false });

Users.hasMany(Notification);
Users.belongsTo(Notification, { as: 'NotificationConnection', constraints: false });

Users.generateHash = password => bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
Users.prototype.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

export default Users;
