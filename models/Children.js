import Sequelize from 'sequelize';
import sequelize from '../config/db/connection';
import Statistic from './statistic';

const Children = sequelize.define('children', {
    name: Sequelize.STRING,
    gender: Sequelize.STRING,
    img: Sequelize.STRING,
    age: Sequelize.FLOAT,
    height: Sequelize.FLOAT,
    weight: Sequelize.FLOAT,
    BMI: Sequelize.FLOAT,
    arm_circumference: Sequelize.FLOAT,
    subscapular_skinfold: Sequelize.FLOAT,
    triceps_skinfold: Sequelize.FLOAT
});

Children.hasMany(Statistic);
Children.belongsTo(Statistic, { as: 'StatisticConnection', constraints: false });

export default Children;
