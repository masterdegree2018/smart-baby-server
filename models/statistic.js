import Sequelize from 'sequelize';
import sequelize from '../config/db/connection';

const Statistic = sequelize.define('statistic', {
    date: {
      type: Sequelize.DATE,
        defaultValue: new Date()
    },
    name: Sequelize.STRING,
    gender: Sequelize.STRING,
    img: Sequelize.STRING,
    age: Sequelize.FLOAT,
    height: Sequelize.FLOAT,
    weight: Sequelize.FLOAT,
    BMI: Sequelize.FLOAT,
    arm_circumference: Sequelize.FLOAT,
    subscapular_skinfold: Sequelize.FLOAT,
    triceps_skinfold: Sequelize.FLOAT
});

export default Statistic;
