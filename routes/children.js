import express from 'express';
import Children from '../models/Children';
import Users from '../models/users';
import Statistic from '../models/statistic';

const router = express.Router();

router.get('/', async (req, res) => {
    const id = Number(req.query.id);

    try {
        if (id) {
            const user = await Users.findOne({ where: { id } });
            const children = await user.getChildren();

            const extendsChildren = children.map(child => {
                return {
                    ...child.dataValues,
                    statistics: child.getStatistic()
                };
            });

            res.status(200)
                .send({ extendsChildren, userId: id });
        } else {
            const children = await Children.findAll();

            res.status(200)
                .send({ children });
        }
    } catch (e) {
        res.status(422)
            .send({ error: e });
    }
});

router.post('/', async (req, res) => {
    const { name, gender, userId } = req.body;

    if (!name || !gender || !userId) {
        return res.status(400)
            .send({ error: 'Id`s user, name and gender text body is required!' });
    }

    try {
        const user = await Users.findOne({
            where: {
                id: userId
            }
        });

        if (user) {
            user.numberOfChildren += 1;
            await user.save();
        }

        const child = await Children.create({
            name,
            gender,
            userId
        });

        res.status(200)
            .send({ child });
    } catch (e) {
        res.status(422)
            .send({ error: e });
    }
});

const getValue = (value) => {
    if (!value) {
        return null;
    }

    if (!isNaN(value)) {
        return parseFloat(value)
            .toFixed(2);
    }

    return value;
};

router.put('/edit/:id', async (req, res) => {
    const id = Number(req.params.id);
    const { data } = req.body;

    if (!id || !data) {
        return res.status(400)
            .send({ error: 'Id`s child and data is required!' });
    }

    try {
        const child = await Children.findOne({
            where: {
                id
            }
        });

        if (child) {
            Object.entries(data)
                .forEach(([key, newValue]) => {
                    child[key] = getValue(newValue);
                });

            await child.save();

            res.status(200)
                .json({ child });
        }

        if (data.statistic) {
            const child = await Children.findOne({
                where: { id },
                attributes: {
                    exclude: ['createdAt', 'updateAt', 'id', 'userId']
                }
            });

            await Statistic.create({
                ...child.dataValues,
                childId: id
            });
        }
    } catch (e) {
        console.log(e);
        res.status(422)
            .send({ error: e });
    }
});

router.delete('/:id', async (req, res) => {
    const id = Number(req.params.id);

    if (!id) {
        return res.status(400)
            .send({ error: 'Id`s child is required!' });
    }

    try {
        await Children.destroy({
            where: {
                id
            }
        });

        res.status(200)
            .send({ status: 'DELETED', id });
    } catch (e) {
        res.status(422)
            .send({ error: e });
    }
});

router.get('/statistics/all/:userId', async (req, res) => {
    const id = Number(req.params.userId);

    try {
        const children = await Children.findAll({
            where: {
                userId: id
            },
            include: [{ model: Statistic }]
        });

        const statistics = (children || []).reduce((stat = {}, child) => {
            return {
                ...stat,
                [child.id]: child.dataValues.statistics
            };
        }, {});

        res.status(200)
            .send({ statistics });
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});

export default router;
