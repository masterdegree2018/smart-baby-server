import express from 'express';
import Users from '../models/users';
import Children from '../models/Children';
import Notification from '../models/notifications';

const router = express.Router();

router.get('/:id', async (req, res) => {
    const id = req.params.id;

    if (!id) {
        return res.status(400)
            .json({ error: 'Id is required!' });
    }
    try {
        const user = await Users.findOne({
            where: { id },
            attributes: {
                exclude: ['password', 'token']
            },
            include: [{ model: Children }, { model: Notification }]
        });

        const enhancedUser = {
            ...user.dataValues,
            createdAt: new Date(user.dataValues.createdAt).toLocaleString()
        };

        res.status(200)
            .json({
                data: enhancedUser
            });

    } catch (e) {
        res.status(500)
            .json({ error: e });
    }
});

router.get('/lists/all', async (req, res) => {
    try {
        const users = await Users.findAll({
            attributes: {
                exclude: ['password', 'token']
            }
        });

        const usersData = users.map(user => {
            delete user.dataValues.token;
            return user;
        });

        res.status(200)
            .json({ data: usersData });
    } catch (e) {
        res.status(500)
            .json({ error: e });
    }
});

router.put('/edit/:id', async (req, res) => {
    const id = req.params.id;
    const { data } = req.body;

    if (!id || !data.email) {
        return res.status(400)
            .json({ error: 'Id and email is required!' });
    }

    try {
        const user = await Users.findOne({
            where: {
                id
            },
            attributes: {
                exclude: ['password', 'token']
            }
        });

        if (user) {
            Object.entries(data)
                .forEach(([key, newValue]) => {
                    user[key] = newValue;
                });

            await user.save();

            return res.status(200)
                .json({ user });
        }

        return userNotExist(res);

    } catch (e) {
        console.log(e);
        res.status(400)
            .json({ error: e });
    }
});

router.delete('/delete/:id', async (req, res) => {
    const id = req.params.id;

    if (!id) {
        return res.status(400)
            .json({ error: 'You have to add id to url' });
    }

    try {

        const user = await Users.find({
            where: {
                id
            }
        });

        if (user) {
            await Users.destroy({
                where: {
                    id
                }
            });

            return res.status(200)
                .json({ status: 'USER_DELETED', userId: id });
        }

        return userNotExist(res);

    } catch (e) {
        res.status(500)
            .json({ error: e });
    }
});

router.get('/all/emails', async (req, res) => {
    try {
        const users = await Users.findAll({
            attributes: {
                exclude: ['password', 'token']
            }
        });

        const emails = users.reduce((all = [], user) => {
            return [
                ...all,
                user.dataValues.email
            ];
        }, []);

        res.status(200)
            .send({ emails });

    } catch (e) {
        console.log(e);
        res.status(500)
            .send({ error: e });
    }
});

const userNotExist = res => res.status(400)
    .json({ error: 'User not exist in db.' });

export default router;
