import express from 'express';

import signup from './signup';
import signin from './signin';
import filesRouters from './API/filesRouters';
import userData from './userData';
import general from './general';
import children from './children';
import password from './password';
import notifications from './notifications';

const router = express.Router();

router.use('/user', signup);
router.use('/user', signin);
router.use('/user', userData);
router.use('/children', children);
router.use('/admin', general);
router.use('/centils/api', filesRouters);
router.use('/password', password);
router.use('/notifications', notifications);

export default router;
