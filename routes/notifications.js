import express from 'express';
import Notifications from '../models/notifications';
import Users from '../models/users';

const router = express.Router();

const sendToAllUsers = (users, message, sender) => {
    return Promise.all(users.map(async user => {
        await Notifications.create({
            message,
            sender,
            userId: user.dataValues.id
        });
    }));
};

router.post('/send', async (req, res) => {
    const { message, selectedEmail, sendToAll, sender } = req.body;

    if (!message && !selectedEmail) {
        res.status(400)
            .json({ error: 'Messages and userId is required!' });
    }

    try {

        if (!sendToAll) {
            const user = await Users.findOne({
                where: {
                    email: selectedEmail
                }
            });

            const admin = await Users.findOne({
                where: {
                    role: 'ADMIN'
                }
            });

            await Notifications.create({
                message,
                sender,
                userId: user.dataValues.id
            });

            if (admin) {
                await Notifications.create({
                    message,
                    sender,
                    userId: admin.dataValues.id
                });
            }
        } else {
            const users = await Users.findAll();
            await sendToAllUsers(users, message, sender);
        }

        res.sendStatus(200);

    } catch (e) {
        console.log(e);
        res.status(500)
            .json({ error: e });
    }
});

router.get('/:id', async (req, res) => {
    const { id } = req.params;

    if (!id) {
        res.status(400)
            .json({ error: 'Messages and userId is required!' });
    }

    try {
        const user = Users.findOne({
            where: {
                id
            }
        });

        if (user) {
            const notifications = await user.getNotification();
            res.send(200)
                .json({ notifications });
        }

        res.send(200)
            .json({ notifications: [] });
    } catch (e) {
        res.status(500)
            .send({ error: e });
    }
});

export default router;
