import express from 'express';
import Users from '../models/users';
import randToken from 'rand-token';

const router = express.Router();

router.post('/login', async (req, res) => {
    const { email, password } = req.body.data;

    const user = await Users.findOne({
        where: {
            email
        }
    });

    if (!user || !user.validPassword(password)) {
        res.status(403)
            .send({ status: 'fail', message: 'Incorrect email or password' });
        return;
    }

    const liveTimeToken = new Date(Date.now() + (12 * 3600000));
    user.token = { token: randToken.generate(24), liveTime: liveTimeToken };
    user.activity = new Date();

    try {
        await user.save();
        const response = {
            ...user.dataValues,
            status: 'SUCCESS'
        };
        res.status(200)
            .json({ ...response });
    } catch (e) {
        res.status(401)
            .json({ status: 'FAIL', message: 'forbidden' + e });
    }
});

export default router;
