import express from 'express';
import axios from 'axios';

import { API_CENTILS_PATH } from './apiPath';

const router = express.Router();

router.get('/admin/files', async (req, res) => {
    try {
        const { data } = await axios.get(`${API_CENTILS_PATH}/admin/files`);
        res.status(200)
            .send({ ...data });
    } catch (e) {
        res.send({ message: e, status: 'ERROR' });
    }
});

router.get('/centils/all', async (req, res) => {
    const { details } = req.query;
    let path = '/centils/types/all';

    if (details === 'true') {
        path = '/centils/types/all?details=true';
    }

    try {
        const { data } = await axios.get(`${API_CENTILS_PATH}${path}`);
        res.status(200)
            .send({ ...data });
    } catch (e) {
        res.send({ message: e, status: 'ERROR' });
    }
});

router.delete('/admin/delete/:fileName', async (req, res) => {
    const { fileName } = req.params;

    if (!fileName) {
        res.status(400)
            .send({ message: 'File name is required!' });
    }

    try {
        const { data } = await axios.delete(`${API_CENTILS_PATH}/admin/delete/${fileName}`);
        res.status(200)
            .send({ ...data });
    } catch (e) {
        res.send({ message: e, status: 'ERROR' });
    }
});


router.put('/admin/edit/:fileName', async (req, res) => {
    const { fileName } = req.params;

    if (!fileName) {
        res.status(400)
            .send({ message: 'File name is required!' });
    }

    try {
        const { data } = await axios.put(`${API_CENTILS_PATH}/admin/edit/${fileName}`, { data: req.body.data });
        res.status(200)
            .send({ ...data });
    } catch (e) {
        res.send({ message: e, status: 'ERROR' });
    }
});

router.post('/admin/percentiles/add', async (req, res) => {
    const { name } = req.body.data;

    if (!name) {
        return res.status(400)
            .send({ message: 'Name is required!' });
    }

    try {
        const { data } = await axios.post(`${API_CENTILS_PATH}/centils/add`, { data: req.body.data });
        res.status(200)
            .send({ ...data });
    } catch (e) {
        res.send({ message: e, status: 'ERROR' });
    }
});

router.post('/admin/percentiles/remove', async (req, res) => {
    const { name } = req.body.data;

    if (!name) {
        return res.status(400)
            .send({ message: 'Name is required!' });
    }

    try {
        const { data } = await axios.post(`${API_CENTILS_PATH}/centils/remove`, { data: req.body.data });
        res.status(200)
            .send({ ...data });
    } catch (e) {
        res.send({ message: e, status: 'ERROR' });
    }
});

router.post('/admin/update', async (req, res) => {
    try {
        const { data = {} } = req.body;
        const { data: { updateCentils } } = await axios.post(`${API_CENTILS_PATH}/admin/update`, { data });
        res.status(200)
            .send({ ...updateCentils });
    } catch (e) {
        res.send({ message: e, status: 'ERROR' });
    }
});

export default router;
