import express from 'express';
import Users from '../models/users';
import { API_CENTILS_PATH } from './API/apiPath';
import axios from 'axios';
import passport from 'passport';
import Children from '../models/Children';

const router = express.Router();

const getNumberOfChildren = (users) => {
    return users.reduce((numberOfChildren, user) => {
        return numberOfChildren + user.numberOfChildren;
    }, 0);
};

const getTheLatestUser = async () => {
    try {
        const latestUser = await Users.findAll({
            limit: 1,
            order: [['createdAt', 'DESC']],
            attributes: {
                exclude: ['password', 'token']
            }
        });

        return latestUser.shift();
    } catch (e) {
        return e;
    }
};

const getTheLatestFile = async () => {
    try {
        const { data } = await axios.get(`${API_CENTILS_PATH}/admin/files/latest`);
        return data.details;
    } catch (e) {
        return e;
    }
};

const getUsedSpaceByFiles = async () => {
    try {
        const { data } = await axios.get(`${API_CENTILS_PATH}/admin/files/space`);
        return data.allSpace;
    } catch (e) {
        return e;
    }
};

router.get('/dashboard',
    passport.authenticate('bearer', { session: false }),
    async (req, res) => {
        try {
            const users = await Users.findAll();
            const children = await Children.findAll();
            console.log(children);

            const response = {
                numberOfUsers: users.length,
                numberOfChildren: children ? children.length : 0,
                latestUser: await getTheLatestUser(),
                latestFile: await getTheLatestFile(),
                usedSpace: await getUsedSpaceByFiles()
            };

            res.status(200)
                .send({ ...response });
        } catch (e) {
            res.status(500)
                .json({ error: e });
        }
    });

export default router;
