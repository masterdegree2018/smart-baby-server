import express from 'express';
import Users from '../models/users';
import { USER_ROLE } from '../utils/constans';
import randToken from 'rand-token';

const router = express.Router();

router.post('/signup', async (req, res) => {
    const { login, email, password } = req.body.data;

    const user = await Users.find({
        where: {
            email
        }
    });

    if (!user) {
        await Users.create({
            uniqueId: randToken.generate(16),
            login,
            email,
            password: Users.generateHash(password),
            role: USER_ROLE.USER,
            numberOfChildren: 0
        });

        res.send({ status: 'SUCCESS' });
    } else {
        res.send({ status: 'FAIL', message: 'User already exist!' });
    }
});

export default router;
