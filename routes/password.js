import express from 'express';
import mailer from '../service/mailer';
import Users from '../models/users';
import randToken from 'rand-token';

const router = express.Router();

router.post('/', async (req, res) => {

    const { email } = req.body;

    if (!email) {
        return res.status(400)
            .json({ error: 'Email is required!' });
    }

    try {
        const user = await Users.findOne({
            where: {
                email
            }
        });

        if (user) {
            await mailer(email, `http://178.62.8.135:9000/rest/password?id=${user.uniqueId}`);
        }

        res.status(200)
            .send({ status: 'OK' });
    } catch (e) {
        res.status(500)
            .send({ error: e });
    }

});

router.post('/reset', async (req, res) => {
    const { id } = req.query;
    const { password } = req.body;

    if (!id) {
        return res.status(400)
            .json({ error: 'Id in link is required!' });
    }

    if (!password) {
        return res.status(400)
            .json({ error: 'Password is required!' });
    }

    console.log(id);
    console.log(password);

    try {
        const user = await Users.findOne({
            where: {
                uniqueId: id
            }
        });

        if (user) {
            user.password = Users.generateHash(password);
            user.uniqueId = randToken.generate(16);
            await user.save();
        } else {
            res.send(200)
                .send({ status: 'Something is wrong!' });
        }

        res.sendStatus(200);
    } catch (e) {
        console.log(e);
        res.status(400)
            .send({ status: 'ERROR' });
    }
});

export default router;
