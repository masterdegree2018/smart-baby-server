import passport from 'passport';
import { Strategy as BearerStrategy } from 'passport-http-bearer';

import User from '../models/users';

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
    const user = await User.findById(id);
    done(null, user);
});

passport.use(
    new BearerStrategy(async (token, done) => {
        const user = await User.find({
            where: {
                token: {
                    token
                }
            }
        });

        if (!user) {
            return done(null, false);
        }

        const tokenStillLive = new Date() < new Date(user.token.liveTime);

        if (tokenStillLive) {
            return done(null, user, { scope: 'all' });
        }

        return done(null, false);
    })
);
