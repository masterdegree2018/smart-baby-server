import nodemailer from 'nodemailer';

async function main(email, link) {
    // Generate SMTP service account from ethereal.email
    let account = await nodemailer.createTestAccount();

    console.log('Credentials obtained, sending message...');

    // NB! Store the account object values somewhere if you want
    // to re-use the same account for future mail deliveries

    // Create a SMTP transporter object
    let transporter = nodemailer.createTransport(
        {
            host: 'smtp.gmail.com',
            service: 'gmail',
            port: 465,
            auth: {
                user: 'darek123551@gmail.com',
                pass: 'campus145'
            },
            secure: true,
            logger: false,
            debug: false // include SMTP traffic in the logs
        },
        {
            // default message fields

            // sender info
            from: 'SmartBaby@gmail.com',
        }
    );

    // Message object
    let message = {
        // Comma separated list of recipients
        to: email,

        // Subject of the message
        subject: 'Reset Password',

        // HTML body
        html:
            '<p>Here\'s the link to reset the password. Clink on:</p>' +
            `<p>${link}</p>`

    };

    let info = await transporter.sendMail(message);

    console.log('Message sent successfully!');
    console.log(nodemailer.getTestMessageUrl(info));

    // only needed when using pooled connections
    transporter.close();
}

export default main;
